<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
  protected $table = 'subscribers';

  public function generateToken() {
    $this->token = str_random(128);
  }

  public static function verify($token) {
    $subscriber = Self::where('token', $token)->where('verified', '0')->first();
    if($subscriber) {
      $subscriber->verified = '1';
      $subscriber->save();
    }
    return $subscriber;
  }

}
