<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscriber;
use App\ResponseBuilder;

use App\Http\Requests\SubscriberAddRequest;
use App\Mail\NewSubscriberMail;

use Mail;

class SubscriberController extends Controller
{
  public function postAdd(SubscriberAddRequest $request) {
    $email = $request->input('email', '');
    $sub = new Subscriber();
    $sub->email = $email;
    $sub->generateToken();
    $sub->save();
    $mail = new NewSubscriberMail($sub);
    Mail::to($email)->send($mail);
    return ResponseBuilder::send(true, '', '');
  }
  
  public function getVerify(Request $request) {
    $token = $request->input('token', '');
    if(Subscriber::verify($token)) {
      return "Verified";
    }
    return redirect('/');
  }
}
