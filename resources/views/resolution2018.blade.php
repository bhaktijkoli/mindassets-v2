<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="shortcut icon" href="images/favicon.ico">

    <title>Tripaco coming soon template</title>

    <!-- Google font -->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Arimo:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    <!-- Material-design-iconic-font -->
    <link rel="stylesheet" type="text/css" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">

    <!-- jquery.fullPage.css -->
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-82330139-1', 'auto');
      ga('send', 'pageview');
    </script>

  </head>

    <body>

    <!-- Preloader -->
      <div id="preloader">
        <div id="status">
          <div class="spinner">
            Loading...
          </div>
        </div>
      </div>
    <!-- End Preloader -->

    <header class="header">
      <nav class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="index.html"><span>T</span>ripaco</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" id="menu">
              <li class="active" data-menuanchor="1stPage"><a href="#1stPage" class="active">Home</a></li>
              <li data-menuanchor="2ndPage"><a href="#2ndPage">subscribe</a></li>
              <li data-menuanchor="3rdPage"><a href="#3rdPage">About</a></li>
              <li data-menuanchor="4thPage"><a href="#4thPage">Services</a></li>
              <li data-menuanchor="5thPage"><a href="#5thPage">Contact</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </header>

    <!--Fixed Bg Image-->
    <div class="background-parallax full-screen">
      <div id="parallaxbg" class="bg-fixed left top"></div>
    </div>
    <!--/Fixed Bg Image-->

    <div id="fullpage">
      <div class="overlay"></div>
        <div class="section" id="section0">
          <div class="container">
            <div class="commen-containe">
              <div class="row">
                <div class="col-md-12">
                  <div class="home-title section-title">
                    <p class="text-center">creative coming soon template</p>
                    <h1 class="text-center">wel come to tripaco coming soon template</h1>

                    <div class="countdown-time animated bounceIn" data-date="2016-01-01 00:00:00" data-timer="900"></div>

                    <div class="scribe">
                        <a href="#2ndPage" class="btn btn-lg btn-subscribe" role="button">Subscribe</a>
                    </div>
                  </div>
                </div><!--End-col-->
              </div><!--End-row-->
            </div><!-- End commen -->
          </div><!--End-container-->
        </div><!-- End section -->

        <div class="section" id="section1">
          <div class="container">
            <div class="commen-containe">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">

                  <div class="subscribe-section section-title">
                    <h1 class="text-center">subscribe to our newsletter</h1>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat.</p>

                    <p class="email">Type your email address & get updates</p>

                    <div class="confirm-message text-center">
                      <form class="text-center" action="http://zoyothemes.us1.list-manage.com/subscribe/post?u=ea5b5414236e0d2e902ea272a&amp;id=ae7bb99217" id="subscribe-form" method="get">
                        <div class="input-group">
                          <input type="email" class="form-control" id="mce-EMAIL" name="EMAIL"  placeholder="Join Newsletter" required>
                          <span class="input-group-btn">
                            <button class="subscribe-submit" type="submit" id="scribebtn" name="scribebtn">Subscribe</button>
                          </span>
                        </div>
                        <label for="mce-EMAIL"></label>
                      </form>
                    </div><!-- End confirm-message -->

                  </div><!-- End subscribe -->

                </div><!-- End col -->
              </div><!-- End row -->
            </div><!-- End commen -->
          </div><!-- End container -->
        </div><!-- End section -->

        <div class="section" id="section2">
          <div class="container">
            <div class="commen-containe">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">

                  <div class="about-section section-title">
                    <h1 class="text-center">About us</h1>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat.</p>
                  </div>

                </div><!-- End col -->
              </div><!-- End row -->

              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="about-chart">
                    <div class="col-sm-4">
                      <div class="countdown">
                        <div class="easy-pie-chart count-panal" data-percent="90">
                          <div class="percent-container">
                            <span class="percent">90</span>
                          </div>
                        </div>
                        <h1>Photoshop</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="countdown">
                        <div class="easy-pie-chart count-panal" data-percent="78">
                          <div class="percent-container">
                            <span class="percent">78</span>
                          </div>
                        </div>
                        <h1>illustrator</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="countdown">
                        <div class="easy-pie-chart count-panal" data-percent="67">
                          <div class="percent-container">
                            <span class="percent">67</span>
                          </div>
                        </div>
                        <h1>Well documented</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      </div>
                    </div>
                  </div>
                </div><!-- End main col -->
              </div><!--End row-->
            </div><!-- End commen -->
          </div><!-- End container -->
        </div><!-- End section -->


      <div class="section" id="section3">
        <div class="container">
          <div class="commen-containe">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">

                <div class="services-section section-title">
                  <h1 class="text-center">services</h1>
                  <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat.</p>
                </div>

              </div><!--end col-->
            </div><!--end row-->

            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="col-sm-4">
                  <div class="service-details">
                    <div class="services-icon">
                      <i class="ion-ios-browsers-outline"></i>
                    </div>
                    <h1 class="services-title">Interface Design</h1>
                    <p class="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, beatae, esse, aspernatur, alias odio numquam incidunt perspiciatis aliquid voluptate sapiente.</p>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="service-details">
                    <div class="services-icon">
                      <i class="ion-ios-infinite-outline"></i>
                    </div>
                    <h1 class="services-title">Clean Code</h1>
                    <p class="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, beatae, esse, aspernatur, alias odio numquam incidunt perspiciatis aliquid voluptate sapiente.</p>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="service-details">
                    <div class="services-icon">
                      <i class="ion-ios-monitor-outline"></i>
                    </div>
                    <h1 class="services-title">Responsive</h1>
                    <p class="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit, beatae, esse, aspernatur, alias odio numquam incidunt perspiciatis aliquid voluptate sapiente.</p>
                  </div>
                </div>
              </div><!-- End col -->
            </div><!--End row-->
          </div><!-- End commen-containe -->
        </div><!-- End container -->
      </div><!-- End section -->


      <div class="section" id="section4">
        <div class="container">
          <div class="commen-containe contact-containe">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="contact-section section-title">
                    <h1 class="text-center">Contact us</h1>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat.</p>
                </div>
              </div><!-- End col-->
            </div><!-- End row -->

            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="form-section">
                  <form class="contact-form" id="ajax-form" action="https://formsubmit.io/send/5974a7c9-d934-4116-8f8c-4b8259bc0489" method="post">
                    <div class="row">

                      <div class="form-group col-md-6">
                        <div class="input-field">
                          <input type="text" class="form-control" id="name2" placeholder="Name" value="">
                          <div class="error" id="err-name" style="display: none;">Please enter name</div>
                        </div>
                      </div>

                      <div class="form-group col-md-6">
                        <div class="input-field">
                          <input type="text" class="form-control" id="email2" placeholder="Email" value="">
                          <div class="error" id="err-emailvld" style="display: none;">Email is not a valid format</div>
                        </div>
                      </div>

                    </div> <!-- End row -->

                    <div class="row">

                      <div class="form-group col-md-12">
                        <div class="input-field">
                          <input type="text" class="form-control" id="subject2" placeholder="Subject" name="subject" value="">
                          <div class="error" id="err-subject" style="display:none">Please enter subject</div>
                        </div>
                      </div>

                    </div><!--End row -->

                    <div class="row">

                      <div class="form-group col-md-12">
                        <textarea class="form-control" name="Message" id="message2" placeholder="Message" rows="6"></textarea>
                          <div class="error" id="err-message" style="display: none;">Please enter message</div>
                      </div>

                    </div><!-- End row -->

                    <div class="row">
                      <div class="col-md-12 text-center">
                        <div id="ajaxsuccess">E-mail was successfully sent.</div>
                        <div class="error" id="err-form" style="display: none;">There was a problem validating the form please check!</div>
                        <div class="error" id="err-timedout">The connection to the server timed out!</div>
                        <div class="error" id="err-state"></div>
                        <button type="submit" class="btn send-button" id="send">Submit</button>
                      </div><!-- End col -->
                    </div><!-- End row-->

                  </form><!-- End form -->
                </div> <!-- form-section -->
              </div><!-- End col-md-12 -->
            </div><!--End row-->
          </div><!-- End commen-containe -->
        </div><!-- End container -->
      </div><!-- End section -->


    </div><!-- Fullpage -->


    <footer class="footer onstart animated" data-animation="fadeInUp" data-animation-delay="800">
      <nav class="social text-center">
        <ul>
          <li><a href="#" class="social-link facebook"><i class="zmdi zmdi-facebook"></i></a></li>
          <li><a href="#" class="social-link twitter"><i class="zmdi zmdi-twitter"></i></a></li>
          <li><a href="#" class="social-link behance"><i class="zmdi zmdi-behance"></i></a></li>
          <li><a href="#" class="social-link dribbble"><i class="zmdi zmdi-dribbble"></i></a></li>
          <li><a href="#" class="social-link linkedin"><i class="zmdi zmdi-linkedin"></i></a></li>
        </ul>
      </nav>

      <p class="copyright">© 2016 Tripaco - All Rights Reserved</p>

    </footer>



    <!-- Js placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- Jquery.counterup.min.js -->
    <script type="text/javascript" src="js/jquery.waypoints.js"></script>
    <script type="text/javascript" src="js/jquery.counterup.min.js"></script>

    <!-- Jquery.ajaxchimp -->
    <script type="text/javascript" src="js/jquery.ajaxchimp.js"></script>

    <!--Modernizr.custom -->
    <script type="text/javascript" src="js/modernizr.min.js"></script>

    <!--Jquery.parallaxmouse.min -->
    <script type="text/javascript" src="js/jquery.parallaxmouse.min.js"></script>

    <!-- Easypiechart.js -->
    <script type="text/javascript" src="js/easypiechart.js"></script>

    <!-- Jquery.easings.min.js -->
    <script type="text/javascript" src="js/jquery.easings.min.js"></script>

    <!-- Jquery.fullPage.js -->
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>

    <!-- TimeCircles.js -->
    <script type="text/javascript" src="js/TimeCircles.js"></script>

    <!-- Common script for all pages -->
    <script type="text/javascript" src="js/main.js"></script>

    <script type="text/javascript">
      $(window).parallaxmouse({
        invert: true,
        range: 400,
        elms: [
            {el: $('#parallaxbg'), rate: 0.1},

        ]
    });
    </script>

  </body>
</html>
