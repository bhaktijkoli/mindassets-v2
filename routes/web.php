<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ResoultionController@get');
Route::get('/subscriber/verify', 'SubscriberController@getVerify');

Route::get('/update', function() {
  chdir(base_path());
  exec('git reset --hard 2>&1', $output);
  exec('git pull origin master 2>&1', $output);
  foreach ($output as $value) {
    echo "$value</br>";
  }
});
